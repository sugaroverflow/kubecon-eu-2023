# KubeCon/CloudNativeCon EU 2023 Code Challenge

## Getting started

Welcome to the KubeCon/CloudNativeCon EU 2023 Code Challenge hosted on CodeChallenge.dev. _We're so glad you're here_. 

Fork this project into your namespace to get started.

## Level 1

The `.gitlab-ci.yml` in this project uses the `alpine:latest` image, since this is a very frequently used image, we want to cut down on the number of pulls from Docker hub by using the [GitLab Dependency Proxy](https://docs.gitlab.com/ee/user/packages/dependency_proxy/). For this level, you will be changing the CI script to use the dependency proxy instead or pulling from upstream. To do that, you will be prefixing the predefined variable `CI_DEPENDENCY_PROXY_GROUP_IMAGE_PREFIX` to the image as shown below:

```yaml
image: ${CI_DEPENDENCY_PROXY_GROUP_IMAGE_PREFIX}/alpine:latest
```

Create a merge request with your changes and this level will be marked as complete when the predefined variable `CI_DEPENDENCY_PROXY_GROUP_IMAGE_PREFIX` is detected in your MR changes.

## Level 2

For level two, enable the GitLab runner to generate a SLSA-2 attestation, you can review [this blog post](https://about.gitlab.com/blog/2022/11/30/achieve-slsa-level-2-compliance-with-gitlab/) on achieving SLSA Level 2 compliance with GitLab to learn more on how to enable it.

## Level 3

For level 3, visit the [Contributing to GitLab](https://about.gitlab.com/community/contribute/) page to get started. There are many things to contribute to: 

* The Ruby on Rails backend
* The Vue-based frontend
* The Go-based services like the GitLab Runner and Gitaly
* The documentation for all of those things
* And more!
